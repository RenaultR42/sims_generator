#include <gmodule.h>

#include "characteristics.h"

int init_char_model(struct char_model *model)
{
	model->list = NULL;
	model->priority = NULL;
	model->low_priority = NULL;
	model->conflicts = NULL;

	model->priority_percentage = 20;
	return 0;
}

void free_char_model(struct char_model *model)
{

}

int init_heritage(struct heritage *inheritance)
{
	inheritance->parent1 = NULL;
	inheritance->parent2 = NULL;

	inheritance->priority_percentage = 30;
	return 0;
}

void free_heritage(struct heritage *inheritance)
{
	g_list_free(inheritance->parent1);
	g_list_free(inheritance->parent2);
}

int init_char_generator(struct char_generator *generator)
{
	init_char_model(&generator->job);
	init_char_model(&generator->trait);
	init_char_model(&generator->skill);
	init_char_model(&generator->aspiration);
	init_char_model(&generator->sexuality);

	init_heritage(&generator->parents);
	return 0;
}

void free_char_generator(struct char_generator *generator)
{
	free_char_model(&generator->job);
	free_char_model(&generator->trait);
	free_char_model(&generator->skill);
	free_char_model(&generator->aspiration);
	free_char_model(&generator->sexuality);

	free_heritage(&generator->parents);
}

/*
 * Get randomly a characteristic from the list
 */
static gchar *get_random_char(GtkTreeModel *list)
{
	GtkTreeIter iter;
	gchar *ret = NULL;
	gint32 random;
	gint32 nb_char;

	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(list), &iter);
	nb_char = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(list), NULL);
	random = g_random_int_range(0, nb_char);

	for (gint32 i = 1; i < random; i++) {
		if (!gtk_tree_model_iter_next(GTK_TREE_MODEL(list), &iter))
			break;
	}

	gtk_tree_model_get(GTK_TREE_MODEL(list), &iter, 0, &ret, -1);
	return ret;
}

/*
 * Count the number of common characteristics from two parents
 */
static gint32 parents_nb_common_char(struct heritage *inheritance)
{
	gint32 count = 0;

	for (GList *parent1 = inheritance->parent1; parent1; parent1 = parent1->next) {
		for (GList *parent2 = inheritance->parent2; parent2; parent2 = parent2->next) {
			if (!g_strcmp0((gchar *)parent1->data, (gchar *)parent2->data))
				count++;
		}
	}

	return count;
}

/*
 * Get randomly one common characteristics from parents.
 */
static gchar *get_common_char_from_parents(struct heritage *inheritance)
{
	const gint32 NB_COMMON_CHAR = parents_nb_common_char(inheritance);
	gint32 random = g_random_int_range(1, NB_COMMON_CHAR + 1);
	gint32 count = 0;

	for (GList *parent1 = inheritance->parent1; parent1; parent1 = parent1->next) {
		for (GList *parent2 = inheritance->parent2; parent2; parent2 = parent2->next) {
			if (!g_strcmp0((gchar *)parent1->data, (gchar *)parent2->data))
				count++;

			if (count == random)
				return (gchar *)parent1->data;
		}
	}

	return NULL;
}

/*
 * Get randomly one characteristic from one parent
 */
static gchar *get_char_from_parent(GList *parent_char)
{
	guint count = g_list_length(parent_char);
	gint32 random = g_random_int_range(0, count);
	GList *charac = g_list_nth(parent_char, random);
	gchar *ret = NULL;

	if (charac)
		ret = (gchar *)charac->data;

	return ret;
}

/*
 * Apply inheritance.
 * Could be a common characteristic (very likely)
 * Could be an uncommon characteristic (unlikely)
 * Could be not done
 */
static gchar *get_char_from_parents(struct heritage *inheritance)
{
	gchar *ret = NULL;
	gint32 has_common_char = parents_nb_common_char(inheritance);
	gint32 random = g_random_int_range(0, 100);

	/* More likely to have an inheritance with common characteristics from parents */
	if (has_common_char && random >= 50) {
		ret = get_common_char_from_parents(inheritance);
	} else if (random >= 70) {
		/* The new characteristic here can come from one parent at once */
		if (random % 2 == 0)
			ret = get_char_from_parent(inheritance->parent1);
		else
			ret = get_char_from_parent(inheritance->parent2);
	}

	return ret;
}

/*
 * Check if a new characteristic + existing characteristics
 * match with a list where two characteristics considered as conflicts / low or high priority
 */
static gboolean find_match(GtkTreeModel *list_pairs, struct sims_characteristics *known_char, gchar *c)
{
	GtkTreeIter iter;
	gboolean found = FALSE;

	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(list_pairs), &iter);

	while (!found) {
		gchar *char1, *char2;

		gtk_tree_model_get(GTK_TREE_MODEL(list_pairs), &iter, 0, &char1, 1, &char2, -1);

		if (!g_strcmp0(c, char1) || !g_strcmp0(c, char2)) {
			for (GList *list = known_char->list; list && !found; list = list->next) {
				gchar *current_char = (gchar *)list->data;

				if (!g_strcmp0(current_char, char1) || !g_strcmp0(current_char, char2))
					found = TRUE;
			}
		}

		g_free(char1);
		g_free(char2);

		if (!gtk_tree_model_iter_next(GTK_TREE_MODEL(list_pairs), &iter))
			break;
	}

	return found;
}

/*
 * Generate a new characteristic
 * Could come from inheritance from parents or a random value
 * Then it checks (optionnal) if a conflict exists. If yes, retry generation
 * Then it checks (optionnal) if new characteristic is concerned by low / high priority
 */
gchar *gen_sim_char(struct char_model *model, struct sims_characteristics *known_traits, struct heritage *parents)
{
	gchar *ret;
	gboolean found = FALSE;

	while (!found) {
		ret = NULL;

		/* Inheritance */
		if (parents)
			ret = get_char_from_parents(parents);

		/* Random value if no inheritance */
		if (!ret)
			ret = get_random_char(model->list);

		/* If conflict is possible */
		if (model->conflicts) {
			/* If no conflict is detected */
			if (!find_match(model->conflicts, known_traits, ret)) {
				if (model->low_priority && model->priority) {
					gint32 probability = g_random_int_range(0, 100);

					/* A high priority is always accepted if generated
					 * A low priority is more likely to be rejected
					 * If not in high / low priority, it could be rejected too but less likely
					 */
					if (find_match(model->priority, known_traits, ret)) {
						found = TRUE;
					} else if (find_match(model->low_priority, known_traits, ret)) {
						if (probability >= model->priority_percentage)
							found = TRUE;
					} else {
						if (probability >= model->priority_percentage * 2)
							found = TRUE;
					}
				} else {
					found = TRUE;
				}
			}
		} else {
			found = TRUE;
		}
	}

	return ret;
}

/* The new characteristic should not be already owned by the sims */
gchar *gen_sim_char_unique(struct char_model *model, struct sims_characteristics *known_traits, struct heritage *parents, struct sims_characteristics *sims_char)
{
	gchar *name;
	gboolean found = FALSE;

	while (!found) {
		name = gen_sim_char(model, known_traits, parents);
		if (!has_characteristics(sims_char, name))
			found = TRUE;
	}

	return name;
}

/* Generate all new characteristics of Sims */
int generate_sims(struct char_generator *gen, struct sims_character *sims)
{
	struct heritage parents_char;
	gint nb_traits = sims->traits.nb_to_generate;
	gint nb_skills = sims->skills.nb_to_generate;

	parents_char.parent1 = sims->parents[0].list;
	parents_char.parent2 = sims->parents[1].list;

	for (gint i = 0; i < nb_traits; i++) {
		if (get_has_parents(sims))
			add_traits(sims, gen_sim_char_unique(&gen->trait, &sims->traits, &parents_char, &sims->traits));
		else
			add_traits(sims, gen_sim_char_unique(&gen->trait, &sims->traits, NULL, &sims->traits));
	}

	for (gint i = 0; i < nb_skills; i++)
		add_skills(sims, gen_sim_char_unique(&gen->skill, &sims->traits, NULL, &sims->skills));

	for (gint i = 0; i < sims->aspirations.max; i++)
		add_aspirations(sims, gen_sim_char_unique(&gen->aspiration, &sims->traits, NULL, &sims->aspirations));

	if (sims->jobs.max)
		add_job(sims, gen_sim_char(&gen->job, &sims->traits, NULL));

	if (can_have_glasses(sims)) {
		gint random = g_random_int_range(0, 100);
		gboolean has_glasses = FALSE;

		if (random < 40)
			has_glasses = TRUE;

		set_glasses(sims, has_glasses);
	}

	if (can_have_sexuality(sims)) {
		gint random = g_random_int_range(0, 100);
		struct sims_sexuality sexuality;

		sexuality.sexual_orientation = SIMS_SEXUALITY_UNKNOWN;
		sexuality.asexual = FALSE;

		if (random < 50)
			sexuality.sexual_orientation = SIMS_SEXUALITY_HETERO;
		else if (random >= 50 && random < 75)
			sexuality.sexual_orientation = SIMS_SEXUALITY_GAY;
		else if (random >= 75)
			sexuality.sexual_orientation = SIMS_SEXUALITY_BI;

		random = g_random_int_range(0, 100);
		if (random < 10)
			sexuality.asexual = TRUE;

		set_sexuality(sims, &sexuality);
	}

	return 0;
}
