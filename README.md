# Sims generator

The purpose of this application is to generate new Sims characteristics, based on Sims 4 game.
It is a graphical application, developped in C and GTK+ technologies.

## Design

There are 3 main files / objects

* Sims which has some characteristics
* A generator to generate new characteristics randomly or based on some info from user
* A window to display information on the screen

## Data

List of traits, skills, jobs, etc. are defined into GtkListStore in .ui file.
It allows a translation, to be sorted, or eventually edited by an user in easier way.

## TODO

* Add translations and selecting the language
* Add Windows installer with all required dependancies
* Add options, to configure the percentage of random for some things (currently hardcoded into the software)
* Be able to edit all GtkListStore directly from graphical software. To be changed by the user without recompiling the software
* Have more flexible GtkListStore, to select eventually some elements based on mods or extensions of the game instead of using all valid values which is relevant only if the player has all DLC of the game.
* Add tests
* Split graphical design and all GtkListStore parts from .ui file
