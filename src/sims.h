#ifndef H_SIMS
#define H_SIMS

#include <stdbool.h>

#include <gmodule.h>

#define NAME_SIZE			128
#define CHARACTERISTICS_NAME_SIZE	128
#define NB_PARENTS			2

enum sims_type {
	SIMS_TYPE_UNKNOWN = -1,
	SIMS_TYPE_ADULT = 0,
	SIMS_TYPE_TEENAGER,
	SIMS_TYPE_CHILD,
	SIMS_TYPE_BABY,
	SIMS_TYPE_CAT,
	SIMS_TYPE_DOG,
};

enum sims_sexual_orientation {
	SIMS_SEXUALITY_UNKNOWN = -1,
	SIMS_SEXUALITY_HETERO = 0,
	SIMS_SEXUALITY_GAY,
	SIMS_SEXUALITY_BI,
	SIMS_SEXUALITY_NO_SEXUALITY,
};

enum sims_flags {
	SIMS_NO_FLAGS = 0,
	SIMS_HAS_PARENTS = 1 << 0,
	SIMS_HAS_GLASSES = 1 << 1,
	SIMS_HAS_SEXUALITY = 1 << 2,
};

/*
 * List of single kind of characteristics
 * Data are array of char, it is the name of the characteristics
 * Some data can come generation from this software, some others can come from users
 */
struct sims_characteristics {
	GList *list;
	gint max;
	gint nb_to_generate;
	gint nb_from_user;
};

struct sims_sexuality {
	enum sims_sexual_orientation sexual_orientation;
	gboolean asexual;
};

/*
 * The name is purely cosmetic.
 * A Sims can have 2 parents, we have a copy of their traits here to be able to do heritance
 * The flag purpose is to define optional stuff like parents, glasses or sexuality.
 * Not all characters can have these elements and we have no other way to define this.
 */
struct sims_character {
	gchar name[NAME_SIZE];
	enum sims_type type;

	struct sims_sexuality sexuality;
	gint flags, flags_capability;

	struct sims_characteristics jobs;
	struct sims_characteristics aspirations;
	struct sims_characteristics traits;
	struct sims_characteristics skills;
	struct sims_characteristics parents[2];
};

void display_sims(struct sims_character *sims);
gchar *type_to_str(enum sims_type type);
gchar *sexuality_to_str(enum sims_sexual_orientation sexuality);

gint init_sims_characteristics(struct sims_characteristics *sims_char);
void free_sims_characteristics(struct sims_characteristics *sims_char);

gint init_sims(struct sims_character *sims, const gchar *name);
void free_sims(struct sims_character *sims);

gint create_dog(struct sims_character *sims, const gchar *name);
gint create_cat(struct sims_character *sims, const gchar *name);
gint create_baby(struct sims_character *sims, const gchar *name);
gint create_child(struct sims_character *sims, const gchar *name);
gint create_teenager(struct sims_character *sims, const gchar *name);
gint create_adult(struct sims_character *sims, const gchar *name);

gint add_parents(struct sims_character *sims, struct sims_character *parent1, struct sims_character *parent2);
gint add_aspirations(struct sims_character *sims, const gchar *name);
gint add_traits(struct sims_character *sims, const gchar *name);
gint add_skills(struct sims_character *sims, const gchar *name);

gint add_job(struct sims_character *sims, const gchar *name);
gint set_glasses(struct sims_character *sims, gboolean has_glasses);
gint set_sexuality(struct sims_character *sims, struct sims_sexuality *sexuality);
gint set_has_parents_property(struct sims_character *sims, gboolean has_parents);

gboolean get_glasses(struct sims_character *sims);
gboolean get_has_parents(struct sims_character *sims);

gboolean can_have_parents(struct sims_character *sims);
gboolean can_have_glasses(struct sims_character *sims);
gboolean can_have_sexuality(struct sims_character *sims);

gboolean has_characteristics(struct sims_characteristics *sims_char, const gchar *name);
void reset_sims(struct sims_character *sims);

#endif
