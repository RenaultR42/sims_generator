/* sims-window.c
 *
 * Copyright 2019 Charles-Antoine Couret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sims-config.h"
#include "sims-window.h"
#include "sims.h"
#include "characteristics.h"

#define NB_TRAITS_MAX 3
#define NB_SKILLS_MAX 8
#define NB_PARENTS    2

struct _SimsWindow {
	GtkApplicationWindow parent_instance;
};

typedef struct _SimsWindowPrivate SimsWindowPrivate;

struct _SimsWindowPrivate {
	/* Matches from .ui file */
	GtkButton *generate_button, *reset_button;
	GtkCheckButton *has_parents_button;
	GtkLabel *status_label;

	GtkRadioButton *dog_button, *cat_button, *baby_button, *child_button;
	GtkRadioButton *teenager_button, *adult_button;

	GtkBox *parent_1_box;
	GtkComboBox *parent_trait_1_1_choice, *parent_trait_1_2_choice, *parent_trait_1_3_choice;

	GtkBox *parent_2_box;
	GtkComboBox *parent_trait_2_1_choice, *parent_trait_2_2_choice, *parent_trait_2_3_choice;

	GtkBox *character_box;
	GtkComboBox *character_trait_1_choice, *character_trait_2_choice;
	GtkComboBox *character_trait_3_choice;
	GtkComboBox *character_job_choice;
	GtkComboBox *character_skill_1_choice, *character_skill_2_choice;
	GtkComboBox *character_skill_3_choice, *character_skill_4_choice;
	GtkComboBox *character_skill_5_choice, *character_skill_6_choice;
	GtkComboBox *character_skill_7_choice, *character_skill_8_choice;
	GtkComboBox *character_aspiration_choice, *character_sexuality_choice;
	GtkCheckButton *character_asexual_choice, *character_glasses_choice;

	GtkLabel *character_trait_label, *character_skill_label;
	GtkLabel *character_job_label;
	GtkLabel *character_aspiration_label, *character_sexuality_label;

	GtkListStore *traits_adult, *traits_teenager, *traits_child, *traits_baby;
	GtkListStore *traits_dog, *traits_cat;

	GtkListStore *traits_cat_conflicts, *traits_dog_conflicts, *traits_human_conflicts;
	GtkListStore *traits_sexuality, *jobs_list;
	GtkListStore *aspirations_adult, *aspirations_child;

	GtkListStore *skills_list, *skills_traits_low_priority, *skills_traits_priority;
	GtkListStore *skills_traits_skills_conflicts, *skills_traits_aspirations_conflicts;

	/* Useful to simplify functions instead of using objects from .ui file */
	GtkComboBox *character_traits_box[NB_TRAITS_MAX];
	GtkComboBox *character_skills_box[NB_SKILLS_MAX];
	GtkComboBox *parents_traits_box[NB_PARENTS][NB_TRAITS_MAX];

	/* Other members */
	struct sims_character sims;
	struct char_generator generator;
};

G_DEFINE_TYPE_WITH_PRIVATE(SimsWindow, sims_window, GTK_TYPE_APPLICATION_WINDOW);

/*
 * Unset values for all widgets, except has_parents button
 */
static void reset_display(SimsWindow *window)
{
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);

	for (gint i = 0; i < NB_TRAITS_MAX; i++)
		gtk_combo_box_set_active(priv->character_traits_box[i], -1);

	for (gint i = 0; i < NB_SKILLS_MAX; i++)
		gtk_combo_box_set_active(priv->character_skills_box[i], -1);

	gtk_combo_box_set_active(priv->character_aspiration_choice, -1);
	gtk_combo_box_set_active(priv->character_sexuality_choice, -1);
	gtk_combo_box_set_active(priv->character_job_choice, -1);

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(priv->character_glasses_choice), FALSE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(priv->character_asexual_choice), FALSE);
}

/*
 * Display or hide widgets which depends on has_parents characteristic
 * And apply this value to sims object
 */
static void set_has_parents(SimsWindow *window, gboolean enable)
{
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);

	gtk_widget_set_child_visible(GTK_WIDGET(priv->parent_1_box), enable);
	gtk_widget_set_child_visible(GTK_WIDGET(priv->parent_2_box), enable);
	gtk_widget_set_child_visible(GTK_WIDGET(priv->has_parents_button), enable);

	set_has_parents_property(&priv->sims, enable);
}

/*
 * Reset widgets which depend on has_parents characteristic
 * Some traits or skills can have different behaviour in that scenario
 */
static void reset_display_parents(SimsWindow *window)
{
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);
	gboolean has_parents = FALSE;

	if (can_have_parents(&priv->sims)) {
		gtk_widget_set_child_visible(GTK_WIDGET(priv->has_parents_button), TRUE);
		has_parents = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(priv->has_parents_button));
	} else {
		gtk_widget_set_child_visible(GTK_WIDGET(priv->has_parents_button), FALSE);
	}

	for (gint parent = 0; parent < NB_PARENTS; parent++) {
		for (gint trait = 0; trait < NB_TRAITS_MAX; trait++)
			gtk_combo_box_set_active(priv->parents_traits_box[parent][trait], -1);
	}

	set_has_parents(window, has_parents);
}

/*
 * Populate Sims object with elements which come from widgets (user information)
 * Could be parents info, traits and skills only
 */
static void get_info_from_interface(SimsWindow *window, struct sims_character *sims)
{
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);
	struct sims_character parents[2];
	gboolean has_parents = FALSE;

	if (can_have_parents(&priv->sims))
		has_parents = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(priv->has_parents_button));

	set_has_parents_property(sims, has_parents);

	if (has_parents) {
		create_adult(&parents[0], "Parent 1");
		create_adult(&parents[1], "Parent 2");

		for (gint parent = 0; parent < NB_PARENTS; parent++) {
			for (gint trait = 0; trait < NB_TRAITS_MAX; trait++)
				add_traits(&parents[parent], gtk_combo_box_get_active_id(priv->parents_traits_box[parent][trait]));
		}

		add_parents(sims, &parents[0], &parents[1]);
	}

	for (gint i = 0; i < priv->sims.traits.max; i++) {
		if (gtk_widget_get_sensitive(GTK_WIDGET(priv->character_traits_box[i])))
			add_traits(sims, gtk_combo_box_get_active_id(priv->character_traits_box[i]));
	}

	for (gint i = 0; i < priv->sims.skills.max; i++) {
		if (gtk_widget_get_sensitive(GTK_WIDGET(priv->character_skills_box[i])))
			add_skills(sims, gtk_combo_box_get_active_id(priv->character_skills_box[i]));
	}
}

/*
 * From a list of characteristics, select the right elements for all comboxes
 * of this kind of chaaracteristics
 */
static void sims_char_to_interface(SimsWindow *window, GtkComboBox **box, gint nb, struct sims_characteristics *sims_char)
{
	GList *list = sims_char->list;
	gint nb_char = g_list_length(list);
	/* An user can forget to select a known characteristic */
	gint nb_from_user = nb_char - sims_char->nb_to_generate;
	/* Due to the max number of characteristics is not always reached, some comboboxes will be empty */
	gint empty_char = sims_char->max - nb_from_user - sims_char->nb_to_generate;

	for (gint i = 0; i < nb && list; i++) {
		/*
		 * First populate info filled by the user
		 * Then populate empty elements
		 * Finished by generated characteristics
		 */
		if (i < nb_from_user) {
			gtk_combo_box_set_active_id(box[i], (gchar *)list->data);
			list = list->next;
		} else if (i < nb_from_user + empty_char) {
			gtk_combo_box_set_active(box[i], -1);
		} else {
			gtk_combo_box_set_active_id(box[i], (gchar *)list->data);
			list = list->next;
		}
	}
}

/*
 * Get info from Sims to display its information on the display
 * Select right element for all comboboxes and checked buttons.
 */
static void sims_to_interface(SimsWindow *window, struct sims_character *sims)
{
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);
	gboolean has_glasses = get_glasses(&priv->sims);
	gboolean has_sexuality = can_have_sexuality(sims);

	sims_char_to_interface(window, priv->character_traits_box, NB_TRAITS_MAX, &priv->sims.traits);
	sims_char_to_interface(window, priv->character_skills_box, NB_SKILLS_MAX, &priv->sims.skills);

	sims_char_to_interface(window, &priv->character_aspiration_choice, 1, &priv->sims.aspirations);
	sims_char_to_interface(window, &priv->character_job_choice, 1, &priv->sims.jobs);

	if (has_sexuality) {
		gtk_combo_box_set_active(priv->character_sexuality_choice, priv->sims.sexuality.sexual_orientation);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(priv->character_asexual_choice), priv->sims.sexuality.asexual);
	}

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(priv->character_glasses_choice), has_glasses);
}

/*
 * Set a new list of characteristics for a combobox
 * Display or not the combobox and the related label if exists
 */
static void combo_update_choice_widget(GtkLabel *label, GtkComboBox *box, GtkListStore *store, gboolean visible)
{
	gtk_widget_set_child_visible(GTK_WIDGET(box), visible);

	if (visible == TRUE && store) {
		gtk_combo_box_set_model(box, GTK_TREE_MODEL(store));
		gtk_combo_box_set_id_column(box, 0);
	}

	if (label)
		gtk_widget_set_child_visible(GTK_WIDGET(label), visible);
}

/*
 * If there are have less characteristics than widgets related to it, hide these extra widgets
 * And set the new list of valid values.
 */
static void update_char_widgets_array_visible(GtkComboBox **box, gint nb, GtkListStore *store, struct sims_characteristics *sims_char)
{
	for (gint i = 0; i < nb; i++) {
		gboolean visible = FALSE;

		if (i < sims_char->max)
			visible = TRUE;

		combo_update_choice_widget(NULL, box[i], store, visible);
	}
}

/*
 * If there are have less characteristics which come from user than widgets related to it
 * set these extra widgets as non sensitive to be not used by the user. These values will be generated.
 */
static void update_char_widgets_array_sensitive(GtkComboBox **box, gint nb, struct sims_characteristics *sims_char)
{
	for (gint i = 0; i < nb; i++) {
		gboolean sensitive = TRUE;

		if ((i + 1) > sims_char->nb_from_user)
			sensitive = FALSE;

		gtk_widget_set_sensitive(GTK_WIDGET(box[i]), sensitive);
		if (!sensitive)
			gtk_combo_box_set_active(box[i], -1);
	}
}

static void update_char_widgets_array(GtkComboBox **box, gint nb, GtkListStore *store, struct sims_characteristics *sims_char)
{
	update_char_widgets_array_visible(box, nb, store, sims_char);
	update_char_widgets_array_sensitive(box, nb, sims_char);
}

/*
 * Refresh the display of the interface according to new characteristics to select
 * according to new sims type.
 */
static void refresh_widgets_character(SimsWindow *window, GtkListStore *jobs, GtkListStore *traits, GtkListStore *skills, GtkListStore *aspirations, GtkListStore *sexuality)
{
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);
	gboolean have_parents_cap, has_glasses_cap, has_sexuality_cap, has_job_cap;
	gboolean has_aspiration = FALSE, has_skills = FALSE;
	struct sims_character *sims;

	have_parents_cap = can_have_parents(&priv->sims);
	has_glasses_cap = can_have_glasses(&priv->sims);
	has_sexuality_cap = can_have_sexuality(&priv->sims);
	has_job_cap = priv->sims.jobs.max ? TRUE : FALSE;
	sims = &priv->sims;

	reset_display_parents(window);
	gtk_widget_set_child_visible(GTK_WIDGET(priv->has_parents_button), have_parents_cap);
	combo_update_choice_widget(priv->character_job_label, priv->character_job_choice, jobs, has_job_cap);

	gtk_widget_set_child_visible(GTK_WIDGET(priv->character_trait_label), TRUE);
	update_char_widgets_array(priv->character_traits_box, NB_TRAITS_MAX, traits, &sims->traits);

	if (sims->skills.max > 0)
		has_skills = TRUE;

	gtk_widget_set_child_visible(GTK_WIDGET(priv->character_skill_label), has_skills);
	update_char_widgets_array(priv->character_skills_box, NB_SKILLS_MAX, skills, &sims->skills);

	if (sims->aspirations.max > 0)
		has_aspiration = TRUE;

	combo_update_choice_widget(priv->character_aspiration_label, priv->character_aspiration_choice, aspirations, has_aspiration);
	combo_update_choice_widget(priv->character_sexuality_label, priv->character_sexuality_choice, sexuality, has_sexuality_cap);
	gtk_widget_set_child_visible(GTK_WIDGET(priv->character_asexual_choice), has_sexuality_cap);

	gtk_widget_set_child_visible(GTK_WIDGET(priv->character_glasses_choice), has_glasses_cap);
}

static void has_parents_callback(gpointer self, GtkCheckButton *button)
{
	SimsWindow *window = SIMS_WINDOW(self);
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);
	GtkToggleButton *checkbox = GTK_TOGGLE_BUTTON(button);
	gboolean enable = gtk_toggle_button_get_active(checkbox);

	set_has_parents(window, enable);
	gtk_widget_set_child_visible(GTK_WIDGET(priv->has_parents_button), TRUE);
	update_char_widgets_array_sensitive(priv->character_traits_box, NB_TRAITS_MAX, &priv->sims.traits);
	update_char_widgets_array_sensitive(priv->character_skills_box, NB_SKILLS_MAX, &priv->sims.skills);
}

/*
 * Populate generator object according to new sims type
 */
static void populate_generator(SimsWindow *window, GtkListStore *jobs, GtkListStore *traits, GtkListStore *traits_conflicts, GtkListStore *skills, GtkListStore *aspirations, GtkListStore *sexuality)
{
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);
	struct char_generator *gen = &priv->generator;

	gen->job.list = GTK_TREE_MODEL(jobs);

	gen->trait.list = GTK_TREE_MODEL(traits);
	gen->trait.conflicts = GTK_TREE_MODEL(traits_conflicts);

	gen->skill.list = GTK_TREE_MODEL(skills);
	gen->skill.priority = GTK_TREE_MODEL(priv->skills_traits_priority);
	gen->skill.low_priority = GTK_TREE_MODEL(priv->skills_traits_low_priority);
	gen->skill.conflicts = GTK_TREE_MODEL(priv->skills_traits_skills_conflicts);

	gen->aspiration.list = GTK_TREE_MODEL(aspirations);
	gen->aspiration.conflicts = GTK_TREE_MODEL(priv->skills_traits_aspirations_conflicts);

	gen->sexuality.list = GTK_TREE_MODEL(sexuality);
}

static void set_dog_mode(SimsWindow *window)
{
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);
	GtkListStore *jobs, *traits, *skills, *aspirations, *sexuality;

	jobs = priv->jobs_list;
	traits = priv->traits_dog;
	skills = priv->skills_list;
	aspirations = NULL;
	sexuality = priv->traits_sexuality;

	populate_generator(window, jobs, traits, priv->traits_dog_conflicts, skills, aspirations, sexuality);
	refresh_widgets_character(window, jobs, traits, skills, aspirations, sexuality);
}

static void set_cat_mode(SimsWindow *window)
{
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);
	GtkListStore *jobs, *traits, *skills, *aspirations, *sexuality;

	jobs = priv->jobs_list;
	traits = priv->traits_cat;
	skills = priv->skills_list;
	aspirations = NULL;
	sexuality = priv->traits_sexuality;

	populate_generator(window, jobs, traits, priv->traits_cat_conflicts, skills, aspirations, sexuality);
	refresh_widgets_character(window, jobs, traits, skills, aspirations, sexuality);
}

static void set_baby_mode(SimsWindow *window)
{
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);
	GtkListStore *jobs, *traits, *skills, *aspirations, *sexuality;

	jobs = priv->jobs_list;
	traits = priv->traits_baby;
	skills = priv->skills_list;
	aspirations = NULL;
	sexuality = priv->traits_sexuality;

	populate_generator(window, jobs, traits, priv->traits_human_conflicts, skills, aspirations, sexuality);
	refresh_widgets_character(window, jobs, traits, skills, aspirations, sexuality);
}

static void set_child_mode(SimsWindow *window)
{
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);
	GtkListStore *jobs, *traits, *skills, *aspirations, *sexuality;

	jobs = priv->jobs_list;
	traits = priv->traits_child;
	skills = priv->skills_list;
	aspirations = priv->aspirations_child;
	sexuality = priv->traits_sexuality;

	populate_generator(window, jobs, traits, priv->traits_human_conflicts, skills, aspirations, sexuality);
	refresh_widgets_character(window, jobs, traits, skills, aspirations, sexuality);
}

static void set_teenager_mode(SimsWindow *window)
{
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);
	GtkListStore *jobs, *traits, *skills, *aspirations, *sexuality;

	jobs = priv->jobs_list;
	traits = priv->traits_teenager;
	skills = priv->skills_list;
	aspirations = priv->aspirations_adult;
	sexuality = priv->traits_sexuality;

	populate_generator(window, jobs, traits, priv->traits_human_conflicts, skills, aspirations, sexuality);
	refresh_widgets_character(window, jobs, traits, skills, aspirations, sexuality);
}

static void set_adult_mode(SimsWindow *window)
{
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);
	GtkListStore *jobs, *traits, *skills, *aspirations, *sexuality;

	jobs = priv->jobs_list;
	traits = priv->traits_adult;
	skills = priv->skills_list;
	aspirations = priv->aspirations_adult;
	sexuality = priv->traits_sexuality;

	populate_generator(window, jobs, traits, priv->traits_human_conflicts, skills, aspirations, sexuality);
	refresh_widgets_character(window, jobs, traits, skills, aspirations, sexuality);
}

/*
 * Create a new Sims according to selected type on the interface
 * refresh the display according to this new type.
 */
static void select_type_callback(gpointer self, GtkRadioButton *button)
{
	SimsWindow *window = SIMS_WINDOW(self);
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);

	free_sims(&priv->sims);
	free_char_generator(&priv->generator);

	reset_display(window);

	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(priv->dog_button))) {
		create_dog(&priv->sims, "Dog");
		set_dog_mode(window);
	} else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(priv->cat_button))) {
		create_cat(&priv->sims, "Cat");
		set_cat_mode(window);
	} else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(priv->baby_button))) {
		create_baby(&priv->sims, "Baby");
		set_baby_mode(window);
	} else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(priv->child_button))) {
		create_child(&priv->sims, "Child");
		set_child_mode(window);
	} else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(priv->teenager_button))) {
		create_teenager(&priv->sims, "Teenager");
		set_teenager_mode(window);
	} else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(priv->adult_button))) {
		create_adult(&priv->sims, "Adult");
		set_adult_mode(window);
	}
}

static void generate_callback(gpointer self, GtkButton *button)
{
	SimsWindow *window = SIMS_WINDOW(self);
	SimsWindowPrivate *priv = sims_window_get_instance_private(window);

	get_info_from_interface(window, &priv->sims);
	generate_sims(&priv->generator, &priv->sims);

	display_sims(&priv->sims);
	sims_to_interface(window, &priv->sims);

	/* When a Sims is generated and displayed, we can reset all characteristics to allow
	 * a new generation */
	reset_sims(&priv->sims);
}

/* Destroy and init a new Sims without changing the current type */
static void reset_callback(gpointer self, GtkButton *button)
{
	select_type_callback(self, NULL);
}

static void sims_window_class_init(SimsWindowClass *klass)
{
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);

	gtk_widget_class_set_template_from_resource(widget_class, "/org/gnome/sims/sims-window.ui");
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, parent_1_box);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, parent_2_box);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_box);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, generate_button);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, reset_button);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, has_parents_button);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, status_label);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, dog_button);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, cat_button);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, baby_button);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, child_button);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, teenager_button);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, adult_button);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, parent_trait_1_1_choice);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, parent_trait_1_2_choice);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, parent_trait_1_3_choice);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, parent_trait_2_1_choice);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, parent_trait_2_2_choice);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, parent_trait_2_3_choice);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_trait_label);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_trait_1_choice);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_trait_2_choice);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_trait_3_choice);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_job_label);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_job_choice);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_skill_label);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_skill_1_choice);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_skill_2_choice);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_skill_3_choice);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_skill_4_choice);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_skill_5_choice);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_skill_6_choice);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_skill_7_choice);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_skill_8_choice);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_aspiration_label);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_aspiration_choice);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_sexuality_label);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_sexuality_choice);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_asexual_choice);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, character_glasses_choice);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, traits_adult);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, traits_teenager);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, traits_child);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, traits_baby);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, traits_dog);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, traits_cat);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, traits_cat_conflicts);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, traits_dog_conflicts);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, traits_human_conflicts);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, traits_sexuality);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, aspirations_adult);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, aspirations_child);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, jobs_list);

	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, skills_list);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, skills_traits_skills_conflicts);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, skills_traits_aspirations_conflicts);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, skills_traits_low_priority);
	gtk_widget_class_bind_template_child_private(widget_class, SimsWindow, skills_traits_priority);
}

static void sims_window_init(SimsWindow *self)
{
	SimsWindowPrivate *priv = sims_window_get_instance_private(self);

	gtk_widget_init_template(GTK_WIDGET(self));

	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(priv->traits_adult), 0, GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID);
	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(priv->traits_teenager), 0, GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID);
	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(priv->traits_child), 0, GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID);
	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(priv->traits_baby), 0, GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID);
	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(priv->traits_dog), 0, GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID);
	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(priv->traits_cat), 0, GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID);

	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(priv->skills_list), 0, GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID);
	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(priv->jobs_list), 0, GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID);
	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(priv->traits_sexuality), 0, GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID);
	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(priv->aspirations_adult), 0, GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID);
	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(priv->aspirations_child), 0, GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID);

	g_signal_connect_object(G_OBJECT(priv->has_parents_button), "toggled", G_CALLBACK(has_parents_callback), self, G_CONNECT_SWAPPED);
	g_signal_connect_object(G_OBJECT(priv->dog_button), "toggled", G_CALLBACK(select_type_callback), self, G_CONNECT_SWAPPED);
	g_signal_connect_object(G_OBJECT(priv->cat_button), "toggled", G_CALLBACK(select_type_callback), self, G_CONNECT_SWAPPED);
	g_signal_connect_object(G_OBJECT(priv->baby_button), "toggled", G_CALLBACK(select_type_callback), self, G_CONNECT_SWAPPED);
	g_signal_connect_object(G_OBJECT(priv->child_button), "toggled", G_CALLBACK(select_type_callback), self, G_CONNECT_SWAPPED);
	g_signal_connect_object(G_OBJECT(priv->teenager_button), "toggled", G_CALLBACK(select_type_callback), self, G_CONNECT_SWAPPED);
	g_signal_connect_object(G_OBJECT(priv->adult_button), "toggled", G_CALLBACK(select_type_callback), self, G_CONNECT_SWAPPED);

	g_signal_connect_object(G_OBJECT(priv->generate_button), "clicked", G_CALLBACK(generate_callback), self, G_CONNECT_SWAPPED);
	g_signal_connect_object(G_OBJECT(priv->reset_button), "clicked", G_CALLBACK(reset_callback), self, G_CONNECT_SWAPPED);

	priv->character_traits_box[0] = priv->character_trait_1_choice;
	priv->character_traits_box[1] = priv->character_trait_2_choice;
	priv->character_traits_box[2] = priv->character_trait_3_choice;

	priv->character_skills_box[0] = priv->character_skill_1_choice;
	priv->character_skills_box[1] = priv->character_skill_2_choice;
	priv->character_skills_box[2] = priv->character_skill_3_choice;
	priv->character_skills_box[3] = priv->character_skill_4_choice;
	priv->character_skills_box[4] = priv->character_skill_5_choice;
	priv->character_skills_box[5] = priv->character_skill_6_choice;
	priv->character_skills_box[6] = priv->character_skill_7_choice;
	priv->character_skills_box[7] = priv->character_skill_8_choice;

	priv->parents_traits_box[0][0] = priv->parent_trait_1_1_choice;
	priv->parents_traits_box[0][1] = priv->parent_trait_1_2_choice;
	priv->parents_traits_box[0][2] = priv->parent_trait_1_3_choice;

	priv->parents_traits_box[1][0] = priv->parent_trait_2_1_choice;
	priv->parents_traits_box[1][1] = priv->parent_trait_2_2_choice;
	priv->parents_traits_box[1][2] = priv->parent_trait_2_3_choice;

	for (gint parent = 0; parent < NB_PARENTS; parent++) {
		for (gint trait = 0; trait < NB_TRAITS_MAX; trait++)
				gtk_combo_box_set_id_column(priv->parents_traits_box[parent][trait], 0);
	}

	/* Init a new Sims in using the default type and display the interface according to it */
	select_type_callback(self, NULL);
}
