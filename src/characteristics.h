#ifndef H_SIMS_CHARACTERISTICS
#define H_SIMS_CHARACTERISTICS

#include <gtk/gtk.h>
#include <glib-object.h>

#include "sims.h"

/*
 * It is a model to generate characteristics.
 * A new characteristic should come from the list.
 * This new characteristic should not match with the conflict list and already defined traits
 * The software balances the result according to low priority and priority lists
 * according to priority percentage.
 * Conflicts, priority and low priority lists must have the first column with traits and the second one with
 * the elements in the list.
 */
struct char_model {
	GtkTreeModel *list;
	GtkTreeModel *priority;
	GtkTreeModel *low_priority;
	GtkTreeModel *conflicts;

	int priority_percentage;
};

/*
 * To generate a new characteristics, we can generate from parents according to percentage
 */
struct heritage {
	GList *parent1;
	GList *parent2;

	int priority_percentage;
};

/*
 * A collection to generate all characteristics from related lists.
 */
struct char_generator {
	struct char_model job;
	struct char_model trait;
	struct char_model skill;
	struct char_model aspiration;
	struct char_model sexuality;

	struct heritage parents;
};

int init_char_model(struct char_model *model);
void free_char_model(struct char_model *model);

int init_heritage(struct heritage *inheritance);
void free_heritage(struct heritage *inheritance);

int init_char_generator(struct char_generator *generator);
void free_char_generator(struct char_generator *generator);

gchar *gen_sim_char(struct char_model *model, struct sims_characteristics *known_traits, struct heritage *parents);
int generate_sims(struct char_generator *gen, struct sims_character *sims);

#endif
