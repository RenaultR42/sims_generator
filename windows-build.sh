#!/bin/sh

BUILD_DIR="build-windows/"
DST_DIR="release_windows/"

mkdir -p $DST_DIR/share/glib-2.0/schemas $DST_DIR/bin $DST_DIR/share/icons/Adwaita/scalable/
meson $BUILD_DIR --cross meson_windows.txt
cd $BUILD_DIR
ninja

cd -

rsync -a /usr/x86_64-w64-mingw32/sys-root/mingw/bin/ --files-from=windows_dependancies.txt $DST_DIR/bin
cp build-windows/src/sims_generator.exe $DST_DIR/bin
cp /usr/x86_64-w64-mingw32/sys-root/mingw/share/glib-2.0/schemas/gschemas.compiled $DST_DIR/share/glib-2.0/schemas/
cp -r /usr/x86_64-w64-mingw32/sys-root/mingw/share/icons/Adwaita/scalable/ $DST_DIR/share/icons/Adwaita/scalable/
cp /usr/x86_64-w64-mingw32/sys-root/mingw/share/icons/Adwaita/index.theme $DST_DIR/share/icons/Adwaita/
