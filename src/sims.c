#include <string.h>
#include <stdio.h>

#include "sims.h"

gchar *type_to_str(enum sims_type type)
{
	gchar *ret = NULL;

	switch (type) {
	case SIMS_TYPE_ADULT:
		ret = "adult";
		break;
	case SIMS_TYPE_TEENAGER:
		ret = "teenager";
		break;
	case SIMS_TYPE_CHILD:
		ret = "child";
		break;
	case SIMS_TYPE_BABY:
		ret = "baby";
		break;
	case SIMS_TYPE_DOG:
		ret = "dog";
		break;
	case SIMS_TYPE_CAT:
		ret = "cat";
		break;
	default:
		ret = "unknown";
		break;
	}

	return ret;
}

gchar *sexuality_to_str(enum sims_sexual_orientation sexuality)
{
	gchar *ret = NULL;

	switch (sexuality) {
	case SIMS_SEXUALITY_HETERO:
		ret = "hetero";
		break;
	case SIMS_SEXUALITY_GAY:
		ret = "gay";
		break;
	case SIMS_SEXUALITY_BI:
		ret = "bi";
		break;
	case SIMS_SEXUALITY_NO_SEXUALITY:
		ret = "no sexuality";
		break;
	default:
		ret = "unknown";
		break;
	}

	return ret;
}

static void display_flags(gint flags)
{
	fprintf(stdout, "Flags: \n");

	if (flags == SIMS_NO_FLAGS) {
		fprintf(stdout, "\tno flags\n");
	} else {
		if (flags & SIMS_HAS_PARENTS)
			fprintf(stdout, "\thas parents\n");
		if (flags & SIMS_HAS_GLASSES)
			fprintf(stdout, "\thas glasses\n");
	}
}

static void display_characteristics(struct sims_characteristics *sims_char)
{
	fprintf(stdout, "number characteristics max: %d\n", sims_char->max);

	for (GList *list = sims_char->list; list; list = list->next)
		fprintf(stdout, "\t%s\n", (gchar *)list->data);
}

/*
 *
 * Display on standard output
 * Useful for debugging.
 */
void display_sims(struct sims_character *sims)
{
	fprintf(stdout, "Sims name: %s\n", sims->name);

	fprintf(stdout, "\ttype: %s\n", type_to_str(sims->type));
	fprintf(stdout, "\tsexuality: %s\n", sexuality_to_str(sims->sexuality.sexual_orientation));
	fprintf(stdout, "\tis asexual: %s\n", sims->sexuality.asexual ? "yes" : "false");

	display_flags(sims->flags);
	fprintf(stdout, "Capability ");
	display_flags(sims->flags_capability);
	fprintf(stdout, "Jobs ");
	display_characteristics(&sims->jobs);
	fprintf(stdout, "Aspirations ");
	display_characteristics(&sims->aspirations);
	fprintf(stdout, "Traits ");
	display_characteristics(&sims->traits);
	fprintf(stdout, "Skills ");
	display_characteristics(&sims->skills);

	if (sims->flags & SIMS_HAS_PARENTS) {
		for (gint i = 0; i < 2; i++) {
			fprintf(stdout, "Parent %d\n", i + 1);
			display_characteristics(&sims->parents[i]);
		}
	}
}

gint init_sims_characteristics(struct sims_characteristics *sims_char)
{
	sims_char->list = NULL;
	sims_char->max = 0;
	sims_char->nb_to_generate = 0;
	sims_char->nb_from_user = 0;

	return 0;
}

void free_sims_characteristics(struct sims_characteristics *sims_char)
{
	g_list_free(sims_char->list);
	sims_char->list = NULL;
}

gint init_sims(struct sims_character *sims, const gchar *name)
{
	strncpy(sims->name, name, NAME_SIZE - 1);

	sims->flags = 0;
	sims->sexuality.sexual_orientation = SIMS_SEXUALITY_UNKNOWN;
	sims->sexuality.asexual = FALSE;

	init_sims_characteristics(&sims->aspirations);
	init_sims_characteristics(&sims->traits);
	init_sims_characteristics(&sims->skills);
	init_sims_characteristics(&sims->jobs);
	init_sims_characteristics(&sims->parents[0]);
	init_sims_characteristics(&sims->parents[1]);

	return 0;
}

void free_sims(struct sims_character *sims)
{
	free_sims_characteristics(&sims->aspirations);
	free_sims_characteristics(&sims->traits);
	free_sims_characteristics(&sims->skills);
	free_sims_characteristics(&sims->parents[0]);
	free_sims_characteristics(&sims->parents[1]);
}

/*
 * Into a list, remove the first element if the list reaches the maximum number
 * of elements.
 */
static GList *check_and_free_first_item(GList *list, gint limit)
{
	if (g_list_length(list) >= limit)
		list = g_list_delete_link(list, list);

	return list;
}

gint create_dog(struct sims_character *sims, const gchar *name)
{
	gint ret = init_sims(sims, name);

	sims->type = SIMS_TYPE_DOG;
	sims->flags_capability = SIMS_NO_FLAGS;
	sims->traits.max = 3;
	sims->traits.nb_to_generate = 3;

	return ret;
}

gint create_cat(struct sims_character *sims, const gchar *name)
{
	int ret = init_sims(sims, name);

	sims->type = SIMS_TYPE_CAT;
	sims->flags_capability = SIMS_NO_FLAGS;
	sims->traits.max = 3;
	sims->traits.nb_to_generate = 3;

	return ret;
}

gint create_baby(struct sims_character *sims, const gchar *name)
{
	gint ret = init_sims(sims, name);

	sims->type = SIMS_TYPE_CAT;
	sims->flags_capability = SIMS_NO_FLAGS;
	sims->traits.max = 1;
	sims->traits.nb_to_generate = 1;

	return ret;
}

gint create_child(struct sims_character *sims, const gchar *name)
{
	gint ret = init_sims(sims, name);

	sims->type = SIMS_TYPE_CHILD;
	sims->flags_capability = SIMS_HAS_GLASSES | SIMS_HAS_PARENTS;

	sims->aspirations.max = 1;
	sims->aspirations.nb_to_generate = 1;

	sims->traits.max = 1;
	sims->traits.nb_to_generate = 1;

	return ret;
}

gint create_teenager(struct sims_character *sims, const gchar *name)
{
	gint ret = init_sims(sims, name);
	gint nb_skills_to_generate = g_random_int_range(2, 5);

	sims->type = SIMS_TYPE_TEENAGER;
	sims->flags += SIMS_HAS_SEXUALITY;
	sims->flags_capability = SIMS_HAS_GLASSES | SIMS_HAS_PARENTS | SIMS_HAS_SEXUALITY;

	sims->aspirations.max = 1;
	sims->aspirations.nb_to_generate = 1;
	sims->traits.max = 2;
	sims->traits.nb_to_generate = 2;
	sims->skills.max = 4;
	sims->skills.nb_to_generate = nb_skills_to_generate;

	return ret;
}

gint create_adult(struct sims_character *sims, const gchar *name)
{
	gint ret = init_sims(sims, name);
	gint nb_skills_to_generate = g_random_int_range(2, 5);

	sims->type = SIMS_TYPE_ADULT;
	sims->flags += SIMS_HAS_SEXUALITY;
	sims->flags_capability = SIMS_HAS_GLASSES | SIMS_HAS_PARENTS | SIMS_HAS_SEXUALITY;

	sims->aspirations.max = 1;
	sims->aspirations.nb_to_generate = 1;
	sims->traits.max = 3;
	sims->traits.nb_to_generate = 3;
	sims->skills.max = 8;
	sims->skills.nb_to_generate = nb_skills_to_generate;
	sims->skills.nb_from_user = 4;
	sims->jobs.max = 1;
	sims->jobs.nb_to_generate = 1;

	return ret;
}

gint add_parents(struct sims_character *sims, struct sims_character *parent1, struct sims_character *parent2)
{
	gint ret = 0;

	free_sims_characteristics(&sims->parents[0]);
	free_sims_characteristics(&sims->parents[1]);

	sims->parents[0].list = g_list_copy(parent1->traits.list);
	sims->parents[1].list = g_list_copy(parent2->traits.list);

	sims->parents[0].max = parent1->traits.max;
	sims->parents[1].max = parent2->traits.max;

	if (!sims->parents[0].list || !sims->parents[1].list)
		ret = -1;

	return ret;
}

static gint set_property(struct sims_character *sims, enum sims_flags property, gboolean enabled)
{
	gint flag = enabled ? property : SIMS_NO_FLAGS;

	sims->flags ^= flag;
	return 0;
}

static gboolean get_capability_flag(struct sims_character *sims, enum sims_flags flag)
{
	gboolean ret = sims->flags_capability & flag ? TRUE : FALSE;

	return ret;
}

static gboolean get_flag(struct sims_character *sims, enum sims_flags flag)
{
	gboolean ret = sims->flags & flag ? TRUE : FALSE;

	return ret;
}

gboolean get_glasses(struct sims_character *sims)
{
	return get_flag(sims, SIMS_HAS_GLASSES);
}

gboolean get_has_parents(struct sims_character *sims)
{
	return get_flag(sims, SIMS_HAS_PARENTS);
}

gboolean can_have_parents(struct sims_character *sims)
{
	return get_capability_flag(sims, SIMS_HAS_PARENTS);
}

gboolean can_have_glasses(struct sims_character *sims)
{
	return get_capability_flag(sims, SIMS_HAS_GLASSES);
}

gboolean can_have_sexuality(struct sims_character *sims)
{
	return get_capability_flag(sims, SIMS_HAS_SEXUALITY);
}

gint set_glasses(struct sims_character *sims, gboolean has_glasses)
{
	return set_property(sims, SIMS_HAS_GLASSES, has_glasses);
}

gint set_sexuality(struct sims_character *sims, struct sims_sexuality *sexuality)
{
	sims->sexuality.sexual_orientation = sexuality->sexual_orientation;
	sims->sexuality.asexual = sexuality->asexual;

	return 0;
}

gint set_has_parents_property(struct sims_character *sims, gboolean has_parents)
{
	gint ret = -1;

	if (can_have_parents(sims)) {
		ret = set_property(sims, SIMS_HAS_PARENTS, has_parents);

		/* The number of traits to generate depends if we generate a Sims with parents or not
		 * Without parents, the character is new, so less characteristics are known
		 * Otherwise, it is Sims who is growing up and the user can select known traits
		 */
		if (!has_parents)
			sims->traits.nb_to_generate = sims->traits.max;
		else
			sims->traits.nb_to_generate = 1;

		sims->traits.nb_from_user = sims->traits.max - sims->traits.nb_to_generate;
	}

	return ret;
}

static gint add_characteristics(struct sims_characteristics *sims_char, const gchar *name)
{
	gint ret = 0;

	/* Append a new characteristic only if it is valid
	 * and we remove the first characteristics if there are more characteristics than
	 * the maximum number of this kind of characterics allowed for this Sims
	 */
	if (name && strlen(name)) {
		sims_char->list = check_and_free_first_item(sims_char->list, sims_char->max);
		sims_char->list = g_list_append(sims_char->list, name);
	} else {
		ret = -1;
	}

	return ret;
}

gboolean has_characteristics(struct sims_characteristics *sims_char, const gchar *name)
{
	gboolean ret = FALSE;

	for (GList *list = sims_char->list; list; list = list->next) {
		if (list->data && !g_strcmp0((gchar *)list->data, name)) {
			ret = TRUE;
			break;
		}
	}

	return ret;
}

gint add_job(struct sims_character *sims, const gchar *name)
{
	return add_characteristics(&sims->jobs, name);
}

gint add_aspirations(struct sims_character *sims, const gchar *name)
{
	return add_characteristics(&sims->aspirations, name);
}

gint add_traits(struct sims_character *sims, const gchar *name)
{
	return add_characteristics(&sims->traits, name);
}

gint add_skills(struct sims_character *sims, const gchar *name)
{
	return add_characteristics(&sims->skills, name);
}

/*
 * Remove all defined characteristics, but not the flags.
 */
void reset_sims(struct sims_character *sims)
{
	gint nb_skills_to_generate = g_random_int_range(2, 5);

	free_sims(sims);
	sims->skills.nb_to_generate = nb_skills_to_generate;
}
